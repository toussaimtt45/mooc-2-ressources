Fiche
**TEST
## Chapitre 1

**Qu'appele t'on un graphe ?**

C'est un enseble fini de points ou sommets et les liens entre ces points.

Exemple de graphe :

    Réseau internet

    Réseau social

    Réseau routier

Ces liens sont le choix d'un sommet S1 de départ et d'un sommet S2 d'arrivée.
Lorsque les liens sont symétriques, c'est à dire lorsque l'existence d'un lien de S1 vers S2 implique l'existence d'un lien de S2 vers S1, le graphe est dit non **orienté**

Des poids peuvent être associés au liens d'un graphe. (orienté ou non), on pârle d'un graphe pondéré.

On appele ordre d'un graphe son nombre de sommets.

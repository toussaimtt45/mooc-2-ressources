# Partie 1

## Sous-partie 1 : texte

Une phrase sans rien  
_Une phrase en italique_  
**Une phrase en gras**  
[Un lien vers fun-mooc.fr](www.fun-mooc.fr)  
Une ligne de `code`

## Sous-partie 2 : listes

**Liste à puce**
- item l
    - sous-item m
    - sous-item m
- item l
- item l

**Liste numérotée**
1 item 1.
2 item 2.
3 item 3.

## Sous-partie 3 : code
```
# Extrait de code
``csqddcefe` 

